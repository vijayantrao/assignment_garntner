using AssignmentGartner.Adapters;
using AssignmentGartner.Models;
using AssignmentGartner.Repositories;
using AssignmentGartner.Services;
using Moq;
using System.Threading.Tasks;
using Xunit;

namespace AssignmentGartner.Tests
{
    public class ProductsServiceTests
    {
        private readonly Mock<IAdapterProvider> _adapterProviderMock;
        private readonly Mock<IProductRespository> _repositoryMock;
        private readonly Mock<IDataAdapter> _adapterMock;
        private ProductService productsService;

        public ProductsServiceTests()
        {
            _adapterProviderMock = new Mock<IAdapterProvider>();
            _adapterMock = new Mock<IDataAdapter>();
            _adapterMock.Setup(x => x.GetData(It.IsAny<string>())).ReturnsAsync(new System.Collections.Generic.List<Product> { new Product { Name = "product1", Categories = "c1,c2,c3" }, new Product { Name = "product2", Categories = "c11,c2,c3", Twitter="@Twiter" } });
            _adapterProviderMock.Setup(x => x.GetAdapter(It.IsAny<Clients>())).Returns(_adapterMock.Object);
            _repositoryMock = new Mock<IProductRespository>();
            
            productsService = new ProductService(_adapterProviderMock.Object, _repositoryMock.Object);
        }

        
        [Theory]
        [InlineData("softwareadvice feed-products/softwareadvice.json")]
        [InlineData("capterra feed-products/capterra.yaml")]
        public async Task ShouldUpdateInventory(string input)
        {
            await productsService.UpdateInventory(input);
            _adapterProviderMock.Verify(x => x.GetAdapter(It.IsAny<Clients>()), Times.Once);
            _adapterMock.Verify(x => x.GetData(It.IsAny<string>()), Times.Once);
            _repositoryMock.Verify(x => x.Save(It.IsAny<Product>()), Times.Exactly(2));
        }

        [Theory]
        [InlineData("feed-products/softwareadvice.json")]
        [InlineData("feed-products/capterra.yaml")]
        public async Task ShouldNotUpdateInventory(string input)
        {
            await productsService.UpdateInventory(input);
            _adapterProviderMock.Verify(x => x.GetAdapter(It.IsAny<Clients>()), Times.Never);
            _adapterMock.Verify(x => x.GetData(It.IsAny<string>()), Times.Never);
            _repositoryMock.Verify(x => x.Save(It.IsAny<Product>()), Times.Never);
        }
        [Fact]
        public async Task ShouldNotCallSaveWhenAdepterDoNotReturnsData()
        {
            const string input = "capterra feed-products/capterra.yaml";
            _adapterMock.Setup(x => x.GetData(It.IsAny<string>())).ReturnsAsync(new System.Collections.Generic.List<Product> {});
            await productsService.UpdateInventory(input);

            _adapterProviderMock.Verify(x => x.GetAdapter(It.IsAny<Clients>()), Times.Once);
            _adapterMock.Verify(x => x.GetData(It.IsAny<string>()), Times.Once);
            _repositoryMock.Verify(x => x.Save(It.IsAny<Product>()), Times.Never);
        }

    }
}