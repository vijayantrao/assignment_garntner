﻿
namespace AssignmentGartner.Models
{
    public class CapterraProduct
    {
        public string Twitter { get; set; }
        public string Name { get; set; }
        public string Tags { get; set; }
    }
}
