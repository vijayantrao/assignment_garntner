﻿using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("AssignmentGartner.Tests")]
namespace AssignmentGartner.Models
{
    public class Product
    {
        public Product()
        {
        }
        public Product(CapterraProduct product)
        {
            if(product != null)
            {
                Name = product.Name;
                Twitter = product.Twitter;
                Categories = product.Tags;
            }                        
        }

        public Product(SoftwareAdviseProduct product)
        {
            if (product != null)
            {
                Name = product.Title;
                Twitter = product.Twitter;
                Categories = string.Join(',', product.Categories.Select(x => new Category { Name = x }));
            }            
        }
        public int Id { get; set; }
        public string Twitter { get; set; }
        public string Name { get; set; }
        public string  Categories { get; set; }
    }
}
