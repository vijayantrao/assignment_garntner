﻿
using System.Text.Json.Serialization;

namespace AssignmentGartner.Models
{
    public class SoftwareAdviseProduct
    {

        [JsonPropertyName("twitter")]
        public string Twitter { get; set; }

        [JsonPropertyName("title")]
        public string Title { get; set; }

        [JsonPropertyName("categories")]
        public List<string> Categories { get; set; }
    }
}
