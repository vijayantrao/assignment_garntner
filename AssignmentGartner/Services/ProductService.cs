﻿using AssignmentGartner.Adapters;
using AssignmentGartner.Models;
using AssignmentGartner.Repositories;

namespace AssignmentGartner.Services
{
    internal class ProductService : IProductService
    {
        private readonly IAdapterProvider _adapterProvider;
        private readonly IProductRespository _repository;
        public ProductService(IAdapterProvider adapterProvider, IProductRespository repository)
        {
            _adapterProvider = adapterProvider;
            _repository = repository;
        }

        public async Task UpdateInventory(string input)
        {
            var (client, path) = GetClientAndPath(input);
            if (client == null || path == null)
            {
                Console.WriteLine("invalid input");
            }
            else
            {
                var adapter = _adapterProvider.GetAdapter(Enum.Parse<Clients>(client));
                var products = await adapter.GetData(path);
                foreach (var product in products)
                {
                    Console.WriteLine($"importing: Name: {product.Name}; Categories::{ product.Categories}; Twitter: {product.Twitter}");
                    await _repository.Save(product);
                }                
            }
        }

        private static (string client, string path) GetClientAndPath(string input)
        {
            if (string.IsNullOrWhiteSpace(input) || !input.Contains(' '))
            {
                return (null, null);
            }
            else
            {
                var inputs = input.Split(" ");
                var client = inputs[0];
                var path = inputs[1];
                return (client, path);
            }
        }
    }
}
