﻿
namespace AssignmentGartner.Services
{
    internal interface IProductService
    {
        Task UpdateInventory(string input);
    }
}