﻿// See https://aka.ms/new-console-template for more information


using AssignmentGartner.Adapters;
using AssignmentGartner.Repositories;
using AssignmentGartner.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

Console.WriteLine("hello");


using IHost host = CreateHostBuilder(args).Build();
var productService = host.Services.GetService<IProductService>();

var input = Console.ReadLine();
productService.UpdateInventory(input);



static IHostBuilder CreateHostBuilder(string[] args) =>
    Host.CreateDefaultBuilder(args)
        .ConfigureServices((_, services) =>
             services.AddScoped<IProductRespository, ProductRespository>()
        .AddScoped<IAdapterProvider, AdapterProvider>()
        .AddScoped<IProductService, ProductService>()
        .AddDbContext<ProductContext>(opt => opt.UseInMemoryDatabase(databaseName: "PoductDatabase"))
        .AddScoped<IProductContext, ProductContext>());








