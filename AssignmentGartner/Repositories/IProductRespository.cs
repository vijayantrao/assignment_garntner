﻿using AssignmentGartner.Models;

namespace AssignmentGartner.Repositories
{
    public interface IProductRespository
    {
        Task Save(List<Product> products);
        Task Save(Product product);
    }
}