﻿using AssignmentGartner.Models;
using Microsoft.EntityFrameworkCore;

namespace AssignmentGartner.Repositories
{
    internal interface IProductContext
    {    
        DbSet<Product> Products { get; set; }
        int SaveChanges();
    }
}