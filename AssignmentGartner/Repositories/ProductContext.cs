﻿using AssignmentGartner.Models;
using Microsoft.EntityFrameworkCore;

namespace AssignmentGartner.Repositories
{
    internal class ProductContext : DbContext, IProductContext
    {
        public DbSet<Product> Products { get; set; }
       
        public ProductContext(DbContextOptions options) : base(options)
        {
        }
        public int SaveChanges()
        {           
           return this.SaveChanges();              
        }
    }
}
