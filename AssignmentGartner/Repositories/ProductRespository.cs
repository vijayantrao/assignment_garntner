﻿
using AssignmentGartner.Models;
using Microsoft.Extensions.Logging;

namespace AssignmentGartner.Repositories
{
    internal class ProductRespository : IProductRespository
    {
        private readonly IProductContext _dbContext;
        private readonly ILogger<ProductRespository> _logger;

        public ProductRespository(IProductContext dbContext,ILogger<ProductRespository> logger)
        {
            _dbContext = dbContext;
            _logger = logger;
        }

        public async Task Save(List<Product> products)
        {
            try
            {
                await _dbContext.Products.AddRangeAsync(products);               
            }
            catch(Exception ex)
            {
                _logger.LogError(ex.Message,ex);
            }
        }

        public async Task Save(Product product)
        {
            try
            {
                await _dbContext.Products.AddAsync(product);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message, ex);
            }
        }
    }
}
