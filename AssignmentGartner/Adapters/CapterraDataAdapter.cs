﻿using AssignmentGartner.Models;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace AssignmentGartner.Adapters
{
    internal class CapterraDataAdapter : IDataAdapter
    {
        public async Task<List<Product>> GetData(string path)
        {
            try
            {
                string data = File.ReadAllText(path);
                var deserializer = new DeserializerBuilder()
                     .WithNamingConvention(CamelCaseNamingConvention.Instance)
                     .Build();

                var products = deserializer.Deserialize<List<CapterraProduct>>(data);
                return await Task.FromResult(products.Select(x => new Product(x)).ToList());
            }
            catch
            {
                return null;
            }
           
        }
    }
}
