﻿using AssignmentGartner.Models;

namespace AssignmentGartner.Adapters
{
    public interface IDataAdapter
    {
        public Task<List<Product>> GetData(string path);
    }
}
