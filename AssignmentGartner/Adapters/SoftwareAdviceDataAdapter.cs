﻿using AssignmentGartner.Models;
using System.Text.Json;

namespace AssignmentGartner.Adapters
{
    internal class SoftwareAdviceDataAdapter : IDataAdapter
    {
        public async Task<List<Product>> GetData(string path)
        {
            try
            {
                var data = await File.ReadAllTextAsync(path);              

                var employeesData = JsonSerializer.Deserialize<SoftwareAdvisorProcuctsWrapper>(data, new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                });

                return employeesData?.Products?.Select(product => new Product(product)).ToList();
            }
            catch
            {
                return null;
            }

           
        }
    }
}
