﻿using AssignmentGartner.Models;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("AssignmentGartner.Tests")]
namespace AssignmentGartner.Adapters
{
    public interface IAdapterProvider
    {
        IDataAdapter GetAdapter(Clients client);
    }
}