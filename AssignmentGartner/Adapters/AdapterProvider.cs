﻿using AssignmentGartner.Models;
namespace AssignmentGartner.Adapters
{
    internal class AdapterProvider : IAdapterProvider
    {
        public IDataAdapter GetAdapter(Clients client)
        {
            switch (client)
            {
                case Clients.softwareadvice: return new SoftwareAdviceDataAdapter();
                case Clients.capterra: return new CapterraDataAdapter();
                default: throw new ArgumentException("No Such client exist");
            }
        }
    }
}
